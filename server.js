var superstatic = require('superstatic/lib/server');
var Handlebars  = require('handlebars');

Handlebars.registerPartial('body', require('./src/partials/body.hbs'));
Handlebars.registerPartial('head', require('./src/partials/head.hbs'));

var fs = require('fs');
var modulesJS = fs.readdirSync('./dist/assets/js/modules');
var modulesCSS = fs.readdirSync('./dist/assets/css/modules');

// Environment variables
var ENV = require('./env.json')[process.env.NODE_ENV || 'local'];

// Static HTTP Server settings
var options = {
  gzip: true,
  port: ENV.port || 3000,
  config: {
    root: './dist',
    routes: {
      '**': 'index.html'
    },
    headers: {
      "**": {
        "Access-Control-Allow-Origin": "*"
      }
    }
  }
};

// Generate html from handlebars template
var index = require('./src/index.hbs')({ modulesCSS: modulesCSS, modulesJS: modulesJS, ENV: JSON.stringify(ENV) });
fs.writeFile('./dist/index.html', index, 'utf-8', function() {

  // Launch server and start listening on port from environment
  var server = superstatic(options);
  server.listen(function() {
    console.info('[INFO]', 'running on port:', options.port, 'env:', ENV.name);
  });
});

