(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var showDialog, showModal;

showModal = require('./show-modal');

showDialog = require('./show-dialog');

module.exports = {
  showModal: showModal,
  showDialog: showDialog
};


},{"./show-dialog":2,"./show-modal":3}],2:[function(require,module,exports){
var MainView, app, layout;

MainView = require('views/main');

app = Backbone.Radio.channel('app');

layout = app.request('layout');

module.exports = function() {
  return layout.dialogs.show(new MainView);
};


},{"views/main":8}],3:[function(require,module,exports){
var MainView, app, layout;

MainView = require('views/main');

app = Backbone.Radio.channel('app');

layout = app.request('layout');

module.exports = function() {
  return layout.modals.show(new MainView);
};


},{"views/main":8}],4:[function(require,module,exports){
var Router, controller, translator;

controller = require('./controller');

Router = require('./router');

translator = Backbone.Radio.channel('translator');

translator.trigger('add', require('./translations.yml'));

new Router({
  controller: controller
});


},{"./controller":1,"./router":5,"./translations.yml":6}],5:[function(require,module,exports){
var InfoRouter,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

InfoRouter = (function(superClass) {
  extend(InfoRouter, superClass);

  function InfoRouter() {
    return InfoRouter.__super__.constructor.apply(this, arguments);
  }

  InfoRouter.prototype.appRoutes = {
    'modal': 'showModal',
    'dialog': 'showDialog'
  };

  return InfoRouter;

})(Marionette.AppRouter);

module.exports = InfoRouter;


},{}],6:[function(require,module,exports){
module.exports = {"es-MX":{"test":"Hello","a-save":"Guardar","a-close":"Cerrar"}};
},{}],7:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = Handlebars;
module.exports = HandlebarsCompiler.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return "<div class=\"bbm-modal__topbar\">\n  <h3 class=\"bbm-modal__title\">"
    + ((stack1 = (helpers.t || (depth0 && depth0.t) || alias2).call(alias1,"test",{"name":"t","hash":{},"data":data})) != null ? stack1 : "")
    + " Stacked modals with Backbone.Marionette</h3>\n</div>\n<div class=\"bbm-modal__section\">\n  <p>Go ahead and open the <a href=\"#\" class=\"open-2\">second modal</a>.</p>\n</div>\n<div class=\"bbm-modal__bottombar\">\n  <button class=\"action-destroy btn btn-warning pull-left\">\n    <div class=\"shortcut-helper-wrapper char-C\">\n      <span>C</span>\n    </div>\n    <i class=\"fa fa-fw fa-arrow-circle-down\"></i>&nbsp; "
    + ((stack1 = (helpers.t || (depth0 && depth0.t) || alias2).call(alias1,"a-close",{"name":"t","hash":{},"data":data})) != null ? stack1 : "")
    + "\n  </button>\n  <button class=\"action-save btn btn-primary\">\n    <div class=\"shortcut-helper-wrapper char-S\">\n      <span>S</span>\n    </div>\n    <i class=\"fa fa-fw fa-save non-waiter\"></i><i class=\"fa fa-fw fa-spin fa-circle-o-notch waiter\"></i>&nbsp; "
    + ((stack1 = (helpers.t || (depth0 && depth0.t) || alias2).call(alias1,"a-save",{"name":"t","hash":{},"data":data})) != null ? stack1 : "")
    + "\n  </button>\n</div>";
},"useData":true});

},{}],8:[function(require,module,exports){
var Content, Modal,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Content = (function(superClass) {
  extend(Content, superClass);

  function Content() {
    return Content.__super__.constructor.apply(this, arguments);
  }

  Content.prototype.template = require('./content.hbs');

  Content.prototype.behaviors = {
    SaveModel: {
      actionClass: 'save',
      saveOptions: {
        wait: true
      },
      destroyView: true,
      shortcut: 'meta + s'
    },
    DestroyModel: {
      actionClass: 'destroy',
      shortcut: 'meta + c'
    },
    Shortcuts: {
      preventDefault: true
    }
  };

  return Content;

})(Marionette.LayoutView);

Modal = (function(superClass) {
  extend(Modal, superClass);

  function Modal() {
    return Modal.__super__.constructor.apply(this, arguments);
  }

  Modal.prototype.template = require('./wrapper.hbs');

  Modal.prototype.childViews = [];

  Modal.prototype.onRender = function() {
    var content;
    content = new Content({
      fromModal: this
    });
    this.childViews.push(content);
    return this.$el.find('>:first-child').html(content.render().el);
  };

  Modal.prototype.onDestroy = function() {
    return _.each(this.childViews, function(view) {
      return view.destroy();
    });
  };

  return Modal;

})(Backbone.Modal);

module.exports = Modal;


},{"./content.hbs":7,"./wrapper.hbs":9}],9:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = Handlebars;
module.exports = HandlebarsCompiler.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "";
},"useData":true});

},{}]},{},[1,2,3,4,5,8]);
