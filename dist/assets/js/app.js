(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var app, translator;

app = Backbone.Radio.channel('app');

translator = Backbone.Radio.channel('translator');

Handlebars.registerHelper('t', function(str) {
  var Translations, lang;
  Translations = translator.request('translations');
  lang = app.request('config:lang' || 'en-US');
  if (!Translations[lang]) {
    return lang + '%' + str + '%';
  }
  if (!Translations[lang][str]) {
    return '%' + str + '%';
  }
  return Translations[lang][str];
});


},{}],2:[function(require,module,exports){
require('./helpers');


},{"./helpers":1}],3:[function(require,module,exports){
var App, Layout, app,
  extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

require('./handlebars');

Layout = require('./layout');

App = (function(superClass) {
  extend(App, superClass);

  function App() {
    return App.__super__.constructor.apply(this, arguments);
  }

  App.prototype.layout = new Layout();

  App.prototype.channel = Backbone.Radio.channel('app');

  App.prototype.initialize = function() {
    this.channel.reply('layout', this.layout);
    this.channel.reply('self', this);
    return this.channel.reply('config:lang', 'es-MX');
  };

  App.prototype.onStart = function() {
    this.layout.render();
    Backbone.Intercept.start();
    Backbone.Intercept.navigate = function(uri, options) {
      return Backbone.history.navigate(uri, options);
    };
    return Backbone.history.start({
      pushState: true
    });
  };

  return App;

})(Marionette.Application);

app = new App();

document.addEventListener("DOMContentLoaded", function() {
  return app.start();
});


},{"./handlebars":2,"./layout":4}],4:[function(require,module,exports){
var extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
  hasProp = {}.hasOwnProperty;

Marionette.Dialogs = (function(superClass) {
  extend(Dialogs, superClass);

  function Dialogs() {
    return Dialogs.__super__.constructor.apply(this, arguments);
  }

  Dialogs.prototype.prefix = 'bbm--dialog';

  Dialogs.prototype.show = function(view, options) {
    view.prefix = 'bbm--dialog';
    return Marionette.Modals.prototype.show.call(this, view, options);
  };

  return Dialogs;

})(Marionette.Modals);

module.exports = (function(superClass) {
  extend(exports, superClass);

  function exports() {
    return exports.__super__.constructor.apply(this, arguments);
  }

  exports.prototype.el = 'body';

  exports.prototype.template = require('./layout.hbs');

  exports.prototype.regions = {
    modals: {
      selector: '#modals',
      regionClass: Marionette.Modals
    },
    dialogs: {
      selector: '#dialogs',
      regionClass: Marionette.Dialogs
    }
  };

  exports.prototype.onRender = function() {
    this.$el.on('keydown', function(e) {
      if (e.keyCode === 91) {
        this.metaPressed = true;
        return $('body').find('.shortcut-helper-wrapper').addClass('shortcut-helper--show');
      }
    });
    return this.$el.on('keyup', function(e) {
      if (e.keyCode === 91) {
        this.metaPressed = false;
        return $('body').find('.shortcut-helper-wrapper').removeClass('shortcut-helper--show');
      }
    });
  };

  return exports;

})(Marionette.LayoutView);


},{"./layout.hbs":5}],5:[function(require,module,exports){
// hbsfy compiled Handlebars template
var HandlebarsCompiler = Handlebars;
module.exports = HandlebarsCompiler.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"modals\"></div>\n<div id=\"dialogs\"></div>\n<a href=\"/modal\">open modal</a>\n&nbsp;&nbsp;&nbsp;&nbsp;\n<a href=\"/dialog\">open dialog</a>";
},"useData":true});

},{}]},{},[3]);
