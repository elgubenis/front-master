watch =
  grunt:
    files: 'Gruntfile.coffee'
    options:
      debounceDelay: 250
      spawn: false

  CSSvendor:
    files: 'src/assets/css/vendor/**/*.css'
    tasks: 'cssmin:vendor'
    options:
      debounceDelay: 250
      livereload: true
      spawn: false

  CSSplugins:
    files: 'src/assets/css/plugins/**/*.css'
    tasks: 'cssmin:plugins'
    options:
      debounceDelay: 250
      livereload: true
      spawn: false

  CSSapp:
    files: 'src/assets/coffee/app/**/*.css'
    tasks: 'cssmin:app'
    options:
      debounceDelay: 250
      livereload: true
      spawn: false

  brwsrfyapp:
    files: [
      'src/assets/coffee/app/**/*.coffee'
      'src/assets/coffee/app/**/*.hbs'
      'src/assets/coffee/app/**/*.yml'
    ]
    tasks: 'browserify:app'
    options:
      debounceDelay: 250
      livereload: true
      spawn: false

  brwsrfyvendor:
    files: ['src/assets/coffee/vendor/**/*.coffee', 'src/assets/coffee/vendor/**/*.js']
    tasks: 'browserify:vendor'
    options:
      debounceDelay: 250
      livereload: true
      spawn: false

  brwsrfyplugins:
    files: ['src/assets/coffee/plugins/**/*.coffee', 'src/assets/coffee/plugins/**/*.js']
    tasks: 'browserify:plugins'
    options:
      debounceDelay: 250
      livereload: true
      spawn: false

module.exports = (modulePaths) ->
  for moduleName in modulePaths
    src = [
      'src/assets/coffee/modules/' + moduleName + '/**/*.coffee'
      'src/assets/coffee/modules/' + moduleName + '/**/*.hbs'
      'src/assets/coffee/modules/' + moduleName + '/**/*.yml'
    ]
    watch['brwsrfy-module-' + moduleName] =
      files: src
      tasks: 'browserify:module-' + moduleName
      options:
        debounceDelay: 250
        livereload: true
        spawn: false

    cssSrc = ['src/assets/coffee/modules/' + moduleName + '/**/*.css']

    watch['CSSmodule-' + moduleName] =
      files: cssSrc
      tasks: 'cssmin:module-' + moduleName
      options:
        debounceDelay: 250
        livereload: true
        spawn: false

  return watch