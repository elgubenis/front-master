browserify =
  app:
    files:
      'dist/assets/js/app.js': 'src/assets/coffee/app/index.coffee'
    options:
      transform: ['caching-coffeeify', 'yamlify', 'hbsfy-global']
      browserifyOptions:
        extensions: '.coffee'
        paths: ['./src/assets/coffee/app']

  vendor:
    files:
      'dist/assets/js/vendor.js': 'src/assets/coffee/vendor/index.coffee'
    options:
      transform: ['caching-coffeeify', 'hbsfy-global']
      browserifyOptions:
        extensions: '.coffee'
        paths: ['./src/assets/coffee/vendor']

  plugins:
    files:
      'dist/assets/js/plugins.js': 'src/assets/coffee/plugins/index.coffee'
    options:
      transform: ['caching-coffeeify', 'hbsfy-global']
      browserifyOptions:
        extensions: '.coffee'
        paths: ['./src/assets/coffee/plugins']

module.exports = (modulePaths) ->

  # define browserifies
  for moduleName in modulePaths
    browserify['module-' + moduleName] =
      files: {}
      options:
        transform: ['caching-coffeeify', 'yamlify', 'hbsfy-global']
        browserifyOptions:
          extensions: '.coffee'
          paths: ['./src/assets/coffee/modules/' + moduleName + '/']

    dest = 'dist/assets/js/modules/' + moduleName + '.js'
    src = 'src/assets/coffee/modules/' + moduleName + '/**/*.coffee'

    browserify['module-' + moduleName].files[dest] = src

  return browserify