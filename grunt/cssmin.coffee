cssmin =
  vendor:
    files:
      'dist/assets/css/vendor.css': 'src/assets/css/vendor/**/*.css'

  plugins:
    files:
      'dist/assets/css/plugins.css': 'src/assets/css/plugins/**/*.css'
  app:
    files:
      'dist/assets/css/app.css': 'src/assets/coffee/app/**/*.css'

module.exports = (modulePaths) ->
  for moduleName in modulePaths
    obj = {}
    obj['dist/assets/css/modules/' + moduleName + '.css'] = 'src/assets/coffee/modules/' + moduleName + '/**/*.css'
    cssmin['module-' + moduleName] =
      files: obj

  return cssmin