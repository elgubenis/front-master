fs = require 'fs'

# get modules
modulePaths = fs.readdirSync './src/assets/coffee/modules'

obj =
  watch: require('./watch')(modulePaths)
  browserify: require('./browserify')(modulePaths)
  cssmin: require('./cssmin')(modulePaths)

module.exports = obj