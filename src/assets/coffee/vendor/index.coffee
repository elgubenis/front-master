window.$            = require 'jquery'
window._            = require 'underscore'
window.Backbone     = require 'backbone'
window.Marionette   = require 'backbone.marionette'
Backbone.Radio      = require 'backbone.radio'
Backbone.Intercept  = require 'backbone.intercept'
require 'backbone-shortcuts'
require 'backbone.modal'

behaviors = Backbone.Radio.channel 'behaviors'

Marionette.Behaviors.behaviorsLookup = ->
  return behaviors.request 'all'