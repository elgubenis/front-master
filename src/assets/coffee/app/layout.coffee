class Marionette.Dialogs extends Marionette.Modals
  prefix: 'bbm--dialog'
  show: (view, options) ->
    view.prefix = 'bbm--dialog'
    Marionette.Modals::show.call @, view, options

class module.exports extends Marionette.LayoutView
  el: 'body'
  template: require './layout.hbs'
  regions:
    modals:
      selector: '#modals'
      regionClass: Marionette.Modals
    dialogs:
      selector: '#dialogs'
      regionClass: Marionette.Dialogs
  onRender: ->
    @$el.on 'keydown', (e) ->
      if e.keyCode is 91
        @metaPressed = true
        $('body').find('.shortcut-helper-wrapper').addClass('shortcut-helper--show')
    @$el.on 'keyup', (e) ->
      if e.keyCode is 91
        @metaPressed = false
        $('body').find('.shortcut-helper-wrapper').removeClass('shortcut-helper--show')