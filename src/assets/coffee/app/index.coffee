require './handlebars'

Layout = require './layout'

class App extends Marionette.Application
  layout: new Layout()
  channel: Backbone.Radio.channel 'app'
  initialize: ->
    @channel.reply 'layout', @layout
    @channel.reply 'self', @
    @channel.reply 'config:lang', 'es-MX'
  onStart: ->
    @layout.render()
    Backbone.Intercept.start()
    Backbone.Intercept.navigate = (uri, options) ->
      Backbone.history.navigate uri, options
    Backbone.history.start
      pushState: true

app = new App()

document.addEventListener "DOMContentLoaded", ->
  app.start()
