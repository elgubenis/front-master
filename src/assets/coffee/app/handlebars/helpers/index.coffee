app = Backbone.Radio.channel 'app'
translator = Backbone.Radio.channel 'translator'

Handlebars.registerHelper 't', (str) ->
  # get all Translations
  Translations = translator.request 'translations'

  # set language
  lang = app.request 'config:lang' or 'en-US'

  if not Translations[lang]
    return lang + '%' + str + '%'
  if not Translations[lang][str]
    return '%' + str + '%'

  return Translations[lang][str]