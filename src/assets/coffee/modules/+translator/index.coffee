extend = require 'deep-extend'

Translations =
  'es-MX':
    'btn-save': 'Guardar'
    'btn-destroy': 'Cancelar'

channel = Backbone.Radio.channel 'translator'

channel.reply 'translations', ->
  return Translations

channel.on 'add', (obj) ->
  extend Translations, obj