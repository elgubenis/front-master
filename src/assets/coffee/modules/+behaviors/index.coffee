
Behaviors = {}

channel = Backbone.Radio.channel 'behaviors'

channel.reply 'all', ->
  return Behaviors

require('./save-model')(Behaviors)
require('./destroy-model')(Behaviors)
require('./shortcuts')(Behaviors)
require('./focus')(Behaviors)