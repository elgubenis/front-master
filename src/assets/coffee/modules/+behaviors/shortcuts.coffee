module.exports = (Behaviors) ->
  class Behaviors.Shortcuts extends Marionette.Behavior
    onRender: ->
      HotKeys.bind(@view.shortcuts, @view, @view.cid, @options)
    onDestroy: ->
      HotKeys.unbind(@view.shortcuts, @view, @view.cid)