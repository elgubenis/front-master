NProgress = require 'nprogress'

module.exports = (Behaviors) ->
  class Behaviors.SaveModel extends Marionette.Behavior
    ui:
      waiters: '.waiter'
      nonwaiters: '.non-waiter'
    initialize: (options) ->
      @trigger = options.trigger or 'click'
      @view.shortcuts = {}
      @saveOptions = options.saveOptions or {}
      @getsDestroyed = options.destroyView or false
      @ui.action = '.action-' + options.actionClass
      if options.shortcut
        @view.shortcuts[options.shortcut] = @action.bind(@)
    onRender: ->
      @ui.waiters.hide()
      @ui.action.on @trigger, @action.bind(@)
    action: ->
      actionTransition.call @
      saveAndDestroy.call(@).then =>
        NProgress.done()
        if @getsDestroyed
          destroyView.call(@)
    onBeforeDestroy: ->
      @view.$el.off()

actionTransition = ->
  @ui.action.prop('disabled', true)
  @ui.nonwaiters.hide()
  @ui.waiters.show()
  NProgress.start()

saveAndDestroy = ->
  model = @view.model
  if model
    return model.save(@saveOptions)
  else
    return {
      then: (cb) ->
        setTimeout ->
          cb()
        , 1000
    }

destroyView = ->
  fromModal = @view.options.fromModal
  view = fromModal or @view
  view.destroy()