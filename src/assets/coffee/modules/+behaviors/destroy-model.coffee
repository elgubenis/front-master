module.exports = (Behaviors) ->
  class Behaviors.DestroyModel extends Marionette.Behavior
    initialize: (options) ->
      @trigger = options.trigger or 'click'
      @ui.action = '.action-' + options.actionClass
      if options.shortcut
        @view.shortcuts[options.shortcut] = @action.bind(@)
    onRender: ->
      @ui.action.on @trigger, @action.bind(@)
    action: ->
      actionTransition.call @
      saveAndDestroy.call @
    onBeforeDestroy: ->
      @view.$el.off()

actionTransition = (e) ->
  @ui.action.prop('disabled')

saveAndDestroy = ->
  fromModal = @view.options.fromModal
  destroyView = fromModal or @view
  destroyView.destroy()