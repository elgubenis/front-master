module.exports = (Behaviors) ->
  class Behaviors.Focus extends Marionette.Behavior
    initialize: (selector) ->
      @selector = selector
    onRender: ->
      _.defer =>
        @view.$el.find(@selector).focus()