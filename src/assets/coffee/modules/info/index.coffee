controller = require './controller'
Router = require './router'

translator = Backbone.Radio.channel 'translator'
translator.trigger 'add', require './translations.yml'

new Router controller: controller