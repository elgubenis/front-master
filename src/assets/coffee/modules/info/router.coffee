class InfoRouter extends Marionette.AppRouter
  appRoutes:
    'modal': 'showModal'
    'dialog': 'showDialog'

module.exports = InfoRouter