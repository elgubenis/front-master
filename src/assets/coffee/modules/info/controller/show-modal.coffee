MainView  = require 'views/main'
app       = Backbone.Radio.channel 'app'
layout    = app.request 'layout'

module.exports = ->
  layout.modals.show new MainView