showModal = require './show-modal'
showDialog = require './show-dialog'

module.exports =
  showModal: showModal
  showDialog: showDialog