class Content extends Marionette.LayoutView
  template: require './content.hbs'
  behaviors:
    SaveModel:
      actionClass: 'save'
      saveOptions:
        wait: true
      destroyView: true
      shortcut: 'meta + s'
    DestroyModel:
      actionClass: 'destroy'
      shortcut: 'meta + c'
    Shortcuts:
      preventDefault: true
    #Focus: '.action-save'

class Modal extends Backbone.Modal
  template: require './wrapper.hbs'
  childViews: []
  onRender: ->
    content = new Content fromModal: @
    @childViews.push content
    @$el.find('>:first-child').html content.render().el
  onDestroy: ->
    _.each @childViews, (view) ->
      view.destroy()

module.exports = Modal