{ watch, browserify, cssmin } = require './grunt'

module.exports = (grunt) ->

  require('jit-grunt')(grunt)

  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')
    watch: watch
    browserify: browserify
    cssmin: cssmin

  grunt.registerTask 'default', 'watch'